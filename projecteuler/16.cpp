#include <iostream>
#include <string>
using namespace std;

#define EXP 1000
#define BASE 2



int main(){
	string n;
	n += 1;
	int r = 0;
	for(int e=1; e<=EXP; ++e){
		for(int i=0; i<n.size(); ++i){
			int t = n[i]*BASE + r;
			n[i] = t%10;
			r = t/10;
		}
		while(r!=0){
			n += r%10;
			r/=10;
		}
	}

	/* Would nomalize and print result
	for(int i=0; i<n.size(); ++i){
		n[i]+='0';
	}
	cout << string(n.rbegin(), n.rend());
	*/

	// Calculates sum of digits
	int ret=0;
	for(int i=0; i<n.size(); ++i)
		ret += n[i];
	cout << ret;
	return 0;
}