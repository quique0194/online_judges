#include <iostream>
#include <set>
using namespace std;

typedef pair<int, int> ii;

int common_digit(int x, int y){
    set<int> s;
    s.insert(y/10); s.insert(y%10);
    if(s.count(x/10)) return x/10;
    if(s.count(x%10)) return x%10;
    return -1;
}
int cancell_digit(int n, int d){
    if(n/10 == d) return n%10;
    return n/10;
}
int gcd(int x, int y){
    return (y==0)? x:gcd(y, x%y);
}
int main(){
    int num=1, den=1;
    for(int i=10; i<100; ++i)
        for(int j=10; j<i; ++j){
            int d = common_digit(j,i);
            if(d!=-1){
                int a = cancell_digit(j,d);
                int b = cancell_digit(i,d);
                if( j%10 && (double)a/b == (double)j/i ){
                    cout << j << "/" << i << endl;
                    num*=j; den*=i;
                }
            }
        }
    int t = gcd(num, den);
    cout << "rpta: " << num/t << "/" << den/t << endl;    
    return 0;
}
