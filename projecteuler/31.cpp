#include <iostream>

using namespace std;

#define COINS 8

int coins[COINS] = {1,2,5,10,20,50,100,200};  

int ways(int total, int min_coin=0){
	if(total < 0) return 0;
	if(total == 0) return 1;
	int ret = 0;
	for(int i=min_coin; i<COINS; ++i)
		ret += ways(total-coins[i], i);
	return ret;
}

int main(int argc, char **argv)
{
	cout << ways(200) << endl;
	return 0;
}