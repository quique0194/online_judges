#include <iostream>
#include <map>
using namespace std;

#define A 101
#define B 101

int main(){
	int m[A][B];

	for(int i=2; i<A; ++i)
		m[i][1] = i;

	for(int i=2; i<A; ++i)
		for(int j=2; j<B; ++j)
			m[i][j] = m[i][j-1] * i;

	map<int, bool> terms;
	for(int i=2; i<A; ++i)
		for(int j=2; j<B; ++j)
			terms[m[i][j]] = 1;

	cout << terms.size();

	return 0;
}