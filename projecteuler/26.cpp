#include <iostream>
#include <cstring>
using namespace std;

#define UPPER_LIMIT 1000

// D < d
int recurring_cycle_size(int D, int d){
	int residue[d];
	memset(residue, 0, sizeof residue);

	int pos = 1;
	while( !residue[D] ){
		residue[D] = pos;
		D = 10*D % d;
		++pos; 
	}
	if( D == 0 ) return 0;
	return pos - residue[D];
}

int main(){
	int max = 0;
	int x = 0;
	for(int i=UPPER_LIMIT-1; i>0 && i>max; --i){
		int t = recurring_cycle_size(1,i);
		if( t > max ){
			max = t;
			x = i;
		}
	}

	cout << x;	

	return 0;
}