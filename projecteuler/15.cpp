#include <iostream>
#include <math.h>
using namespace std;

int main(){
	int n;
	cin >> n;

	double toret = 1;
	for(int i=1; i<=n; ++i){
		toret *= (double)(i+n)/(i);
	}

	cout << (long long int)floor(toret+0.5);
	return 0;
}