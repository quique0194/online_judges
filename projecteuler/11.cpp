#include <iostream>
#include <time.h>

using namespace std;

#define N 20
#define K 4

int get_max_hor(int grid[N][N]){
    int max = 0;

    for(int f=0; f<N; ++f){
        for(int i=0; i<=N-K; ++i){
            int prod = 1;
            for(int j=0; j<K; ++j){
                prod *= grid[f][i+j];
            }
            if(prod > max){
                max = prod;
            }
        }
    }

    return max;
}

int get_max_ver(int grid[N][N]){
    int max = 0;

    for(int c=0; c<N; ++c){
        for(int i=0; i<=N-K; ++i){
            int prod = 1;
            for(int j=0; j<K; ++j){
                prod *= grid[i+j][c];
            }
            if(prod > max){
                max = prod;
            }
        }
    }

    return max;
}

int get_max_down_diag(int grid[N][N]){
    int max = 0;

    for(int f=0; f<=N-K; ++f){
        for(int c=0; c<=N-K; ++c){
            int prod =1;
            for(int i=0; i<K; ++i){
                prod *= grid[f+i][c+i];
            }
            if(prod > max){
                max = prod;
            }
        }
    }

    return max;
}

int get_max_up_diag(int grid[N][N]){
    int max = 0;

    for(int f=K-1; f<N; ++f){
        for(int c=0; c<=N-K; ++c){
            int prod =1;
            for(int i=0; i<K; ++i){
                prod *= grid[f-i][c+i];
            }
            if(prod > max){
                max = prod;
            }
        }
    }

    return max;
}

int main()
{
    clock_t t0 = clock();

    int grid[N][N];

    for(int i=0; i<N; ++i){
        for(int j=0; j<N;++j){
            cin >> grid[i][j];
        }
    }

    cout << get_max_hor(grid) << endl;
    cout << get_max_ver(grid) << endl;
    cout << get_max_down_diag(grid) << endl;
    cout << get_max_up_diag(grid) << endl;
    
    return 0;
}
