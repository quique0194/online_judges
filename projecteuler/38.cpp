#include <iostream>
#include <set>
using namespace std;

bool condition(int x){
    set<int> digits;
    for(int i=1; i<=2 || digits.size()<9; ++i){
        int m = x*i;
        while(m){
            int t = m%10;
            if(t==0 || digits.count(t)) return 0;
            digits.insert(t);
            m/=10;
        }
    }
    return 1;
}

int main(){
    int i = 100000;
    while(i-- && i>0){
        if(condition(i)){
            cout << i << endl;
            cout << i*1 << i*2 << endl;
            break;
        }
    }
    return 0;
}
