#include <iostream>

using namespace std;

#define MAX 354294
#define E 5

int power[10];

int pow(int b, int e){
	int ret = 1;
	for(int i=0; i<e; ++i)
		ret *= b;
	return ret;
}

void init(){
	for(int i=0; i<10; ++i)
		power[i] = pow(i,E);
}

int sum_of_digit_powers(int x){
	int sum = 0;
	while(x){
		sum += power[x%10];
		x /= 10;
	}
	return sum;
}

int main(){
	init();
	int ret = 0;
	for(int i=2; i<=MAX; ++i)
		if(sum_of_digit_powers(i)==i)
			ret += i;
	cout << ret;
	return 0;
}