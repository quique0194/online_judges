#include <iostream>
#include <string.h>
#include <stdlib.h>
using namespace std;

#define N 1000000

int mem[N];


int chain_size(long long int n){
	if(n<N){
		if(mem[n])
			return mem[n];
	}		
	int cont=0;
	while(~n&1){
		n /= 2;
		++cont;
	}
	if( n!=1 ){
		n = (3*n+1)/2;
		cont+=2;
	}
	int rpta = cont+chain_size( n );
	if(n<N){
		mem[n]=rpta;
	}
	return rpta;
}

int main(){
	memset(mem,0, sizeof mem);
	mem[1]=1;
	
	int max=1;
	int toret=1;
	for(long long int i=2; i<N; ++i){
		int cs = chain_size(i);
		if(cs>max){
			toret=i;
			max=cs;
		}			
	}

	cout << toret;
	return 0;
}