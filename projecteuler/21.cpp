#include <iostream>

using namespace std;

#define N 10001

int d(int n){
	int sum = 0;
	int i;
	for(i=2; i*i<n; ++i){
		if( n%i == 0 ){
			sum += i + n/i;
		}
	}
	if( i*i == n )
		sum += i;
	return sum+1;
}


int main(){
	int D[N];
	
	for(int i=2; i<N; ++i)
		D[i] = d(i);

	int sum = 0;
	for(int i=2; i<N; ++i){
		if( D[i] >= N ) continue;
		if( D[D[i]] == i && i!=D[i])
			sum += i;
	}

	cout << sum;

	return 0;
}