#include <iostream>

using namespace std;


int main(){
	
	// from 1 to 9
	int one_nine = 3+3+5+4+4+3+5+5+4;
	// from 1 to 19:
	int ten_nineteen = 3+6+6+8+8+7+7+9+8+8;
	// from 20 to 99:
	int twenty_ninetynine = 10*(6+6+5+5+5+7+6+6) + 8*one_nine;
	// from 100 to 999
	int oneh_ninehninetynine = 100*(13+13+15+14+14+13+15+15+14) - 9*3 + 9*(one_nine+ten_nineteen+twenty_ninetynine) ;
	// 1000
	int onethousand = 11;
	
	cout << one_nine + ten_nineteen + twenty_ninetynine + oneh_ninehninetynine + onethousand;
	return 0;
}