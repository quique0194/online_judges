#include <cstdio>
#include <string>
#include <sstream>
#include <set>
using namespace std;

#define MAX 100

bool is_pandigital(const string &n){
    if(n.size()!=9) return 0;
    int digits = 0;
    for(int i=0; i<n.size(); ++i)
        if( digits & (1<<(n[i]-'0')) ) return 0;
        else digits |= (1<<(n[i]-'0'));
    return ~digits&1;
}

int main(){
	set<int> s;
    for(int i=1; i<MAX; ++i){
        for(int j=i+1; ; ++j){
            stringstream is;
            is << i << j << i*j;
            if(is.str().size()>9) break;
            if(is_pandigital(is.str())) s.insert(i*j);
        }
    }
    int sum = 0;
    for(set<int>::iterator it = s.begin(); it!=s.end(); it++)
        sum += *it;
    printf("%d\n", sum);
	return 0;
}
