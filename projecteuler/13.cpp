#include <iostream>
#include <sstream>

using namespace std;

#define N 100	// Numbers
#define D 50	// Digits

int main(){
	char numbers[N][D];
	for(int i=0; i<N; ++i){
		cin >> numbers[i];
	}

	stringstream res;

	int r = 0;
	for(int i=D-1; i>=0; --i){
		int col_sum=r;
		for(int j=0; j<N; ++j){
			col_sum += numbers[j][i] - '0';
		}
		r = col_sum /10;
		res << col_sum % 10;
	}
	while(r){
		res << r%10;
		r/=10;
	}
	string str = res.str();
	cout << string(str.rbegin(),str.rend());
	return 0;
}