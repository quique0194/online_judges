#include <iostream>
#include <math.h>
#include <string.h>
using namespace std;

int p[1001];

int main(){
    memset(p, 0, sizeof p);
    for(int i=1; i<1000; ++i) for(int j=i; i+2*j<1000; ++j){
        int h = i*i + j*j;
        double sh = sqrt(h);
        if( sh == (double)floor(sh) ){
            int sum = i+j+sh;
            if(sum<=1000) p[sum]++;
        }
    }
    int max = 0;
    for(int i=1; i<1001; ++i)
        if(p[i]>p[max]) max = i;
    cout << max << endl;
    return 0;
}
