#include <iostream>
#include <cstring>
using namespace std;

#define A 1000
#define B 1000
#define PRIMES 100000

bool is_p[PRIMES] = {0};

void sieve(){
	memset(is_p,1,sizeof is_p);
	for(int i=2; i*i<PRIMES; ++i)
		if(is_p[i])
			for(int j=i*i; j<PRIMES; j+=i)
				is_p[j] = 0;				
}

int eval(int a, int b, int n){
	return n*n + a*n + b;
}

bool is_prime(int x){
	if(x<2) return 0;
	return is_p[x];
}

int main(){
	sieve();

	int max = 0;
	int aa;
	int bb;

	for(int b = 2; b < B; ++b)
		if( is_prime(b) )
			for(int a = 1-A; a < A; ++a)
			{
				int n=1;
				while( is_prime( eval(a,b,n) ) ) ++n;
				if( n > max ){
					max = n;
					aa = a;
					bb = b;
				}
			}

	cout << aa*bb;

	return 0;
}