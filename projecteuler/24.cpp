#include <iostream>
#include <string>
using namespace std;

int factorial(int n){
	int ret=1;
	for(int i=2; i<=n; ++i)
		ret *= i;
	return ret;
}

string nth_permutation(string list, int n){
	if( list.size() == 1)
		return list;
	int f = factorial(list.size()-1);
	int pos = n/f;
	char c = list[pos];
	list.erase(pos,1);
	return string(1, c) + nth_permutation(list, n%f);
}

int main(){
	cout << nth_permutation("0123456789", 999999);
	return 0;
}