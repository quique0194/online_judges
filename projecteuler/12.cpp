#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;

// get nth triangular number
int get_tn(int n){
	return (n*n+n)/2;
}


int count_divisors(int n){
	int toret = 1;
	int limit = floor(sqrt(n));
	for(int i=2; i<=limit && n!=1; ++i){
		int t=0;
		while(n%i == 0){
			n /= i;
			t++;
		}
		toret *= t+1;
	}
	return (n!=1)? 2*toret:toret;
}

// count divisors of nth triangular number
int count_divisors_tn(int n){
	int m = n+1;
	if( ~n&1 ) n/=2;
	if( ~m&1 ) m/=2;
	return count_divisors(n)*count_divisors(m);
}

int main(){
	int umbral;
	cin >> umbral;
	
	int i=1;
	while(count_divisors_tn(i)<=umbral){
		++i;
	}
	cout << get_tn(i) << endl;
	return 0;
}