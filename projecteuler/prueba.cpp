#include <iostream>

using namespace std;

int sum_pdivisors(int n){
	int nn = n;
	int prod = 1;
	for(int i=2; i*i<=n; ++i){
		int p = 1;
		while( n%i == 0 ){
			p = p*i + 1;
			n /= i;
		}
		prod *= p;
	}
	if( n > 1 )
		prod *= n+1;
	return prod - nn;
}

int main(){
	for( int i =2; i<20; ++i)
		cout << i << "   " << sum_pdivisors(i) << endl;
	return 0;
}