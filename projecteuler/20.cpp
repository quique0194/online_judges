#include <iostream>
#include <string>
using namespace std;

string factorial(int n){
	string s;
	s += 1;
	int r = 0;
	for(int i=2; i<=n; ++i){
		for(int j=0; j<s.size(); ++j){
			int t = r + s[j]*i;
			s[j] = t%10;
			r = t/10;
		}
		while(r){
			s += r%10;
			r/=10;
		}
	}
	for(int i=0; i<s.size(); ++i)
		s[i]+='0';
	return string(s.rbegin(),s.rend());
}

int main(){
	int n;
	cin >> n;
	string r = factorial(n);
	int sum = 0;
	for(int i=0; i<r.size(); ++i)
		sum+=r[i]-'0';

	cout << sum;

	return 0;
}