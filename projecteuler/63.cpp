#include <iostream>
#include <algorithm>
#include <fstream>
using namespace std;

#define HEIGHT 100

int main(){
	int pyramid[HEIGHT][HEIGHT];
	ifstream file("triangle.txt");
	for(int i=0; i<HEIGHT; ++i){
		for(int j=0; j<=i; ++j){
			file >> pyramid[i][j];
		}
	}
	file.close();

	for(int i=HEIGHT-2; i>=0; --i){
		for(int j=0; j<=i; ++j){
			pyramid[i][j] = pyramid[i][j] + max(pyramid[i+1][j], pyramid[i+1][j+1]); 
		}
	}

	cout << pyramid[0][0];
	return 0;
}