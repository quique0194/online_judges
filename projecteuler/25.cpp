#include <iostream>
#include <string>
using namespace std;

#define MIN_DIGITS 1000

string sum(string& a, string& b){
	if( a.size() > b.size() )
		return sum(b, a);
	int r=0;
	string ret;
	for( int i=0; i<a.size(); ++i ){
		int t = a[i]+b[i]+r;
		ret += t%10;
		r = t/10;
	}
	for( int i=a.size(); i<b.size(); ++i ){
		int t = b[i]+r;
		ret += t%10;
		r = t/10;
	}
	while(r){
		ret += r%10;
		r /=10;
	}
	return ret;
}

int main(){

	int j=2;
	string fi, fj;
	fi += 1;
	fj += 1;
	while( fj.size() < MIN_DIGITS ){
		string t = fj;
		fj = sum(fj, fi);
		fi = t;
		++j;
	}
	cout << fj << endl;
	cout << j;

	return 0;
}