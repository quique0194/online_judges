// Alternative way to 15.cpp

#include <iostream>

using namespace std;

#define N 21

long long int mem[N][N];

void fill_mem(){
	for(int i=0; i<N; ++i){
		mem[0][i]=1;
		mem[i][0]=1;
	}
	for(int i=1; i<N; ++i){
		for(int j=1; j<N; ++j){
			mem[i][j] = mem[i-1][j] + mem[i][j-1];
		}
	}
}

int main(){
	fill_mem();
	cout << mem[20][20];
	return 0;
}