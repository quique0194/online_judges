#include <iostream>

using namespace std;

int fact[10];

void precalc(){
    fact[0] = 1;
    for(int i=1; i<10; ++i)
        fact[i] = fact[i-1]*i;
}
int sum_digit_factorial(int x){
    int sum = 0;
    while(x){
        sum += fact[x%10];
        x /= 10;
    }
    return sum;
}
int main(){
    precalc();
    int sum = 0;
    for(int i=3; i<100000000; ++i)
        if(sum_digit_factorial(i) == i){
            cout << i << endl;
            sum += i;
        }
    cout << "sum: " << sum << endl;
    return 0;
}
