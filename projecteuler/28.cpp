#include <iostream>

using namespace std;

int sum_of_diagonals(int n){
	if(~n&1 || n<1) return -1;
	int sum=0;
	for(int i=n; i>1; i-=2)
		sum += 4*i*i - 6*i + 6;
	return sum+1;
}

int main(){

	cout << sum_of_diagonals(1001);

	return 0;
} 