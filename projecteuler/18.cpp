#include <iostream>
#include <algorithm>
using namespace std;

#define HEIGHT 15

int main(){
	int pyramid[HEIGHT][HEIGHT];
	for(int i=0; i<HEIGHT; ++i){
		for(int j=0; j<=i; ++j){
			cin >> pyramid[i][j];
		}
	}

	for(int i=HEIGHT-2; i>=0; --i){
		for(int j=0; j<=i; ++j){
			pyramid[i][j] = pyramid[i][j] + max(pyramid[i+1][j], pyramid[i+1][j+1]); 
		}
	}

	cout << pyramid[0][0];
	return 0;
}