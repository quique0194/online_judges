#include <iostream>
#include <vector>
using namespace std;

#define UPPER_LIMIT 28123

int sum_pdivisors(int n){
	int nn = n;
	int prod = 1;
	for(int i=2; i*i<=n; ++i){
		int p = 1;
		while( n%i == 0 ){
			p = p*i + 1;
			n /= i;
		}
		prod *= p;
	}
	if( n > 1 )
		prod *= n+1;
	return prod - nn;
}


int main(){

	bool abundant[UPPER_LIMIT];

	for(int i=2; i<UPPER_LIMIT; ++i)
		if( !abundant[i] )
			if( sum_pdivisors(i) > i )
				for(int m = i; m < UPPER_LIMIT; m += i)
					abundant[m] = 1;

	vector<int> abundants;

	for( int i=2; i<UPPER_LIMIT; ++i )
		if( abundant[i] )
			abundants.push_back(i);


	bool sum_of_abundants[UPPER_LIMIT];

	for(int i=0; i<abundants.size(); ++i)
		for(int j=i; j<abundants.size(); ++j){
			int s = abundants[i] + abundants[j];
			if( s >= UPPER_LIMIT ) break;
			sum_of_abundants[s] = 1;
		}
			


	int sum=0;
	for(int i=1; i<UPPER_LIMIT; ++i)
		if( !sum_of_abundants[i] )
			sum += i;

	cout << sum;

	return 0;
}
