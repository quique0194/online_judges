#include <iostream>

using namespace std;

#define FIRST_YEAR 1900
#define YEARS 101
#define MONTHS 12

bool is_leap(int year){
	if( year%400 == 0 )
		return 1;
	if( year%100 == 0 )
		return 0;
	if( year%4 == 0 )
		return 1;
	return 0;
}


int months[2][MONTHS] = {{0,3,3,6,1,4,6,2,5,0,3,5},{0,3,4,0,2,5,0,3,6,1,4,6}};
int year_days[2] = {1,2};

int first_day[YEARS];

void fill_first_days(int fday){
	first_day[0] = fday;

	for(int i=1; i<YEARS; ++i){
		first_day[i] = ( first_day[i-1] + year_days[is_leap(FIRST_YEAR+i-1)] ) % 7;
	}
}

int get_week_day(int day, int month, int year){
	return ( first_day[year-FIRST_YEAR] + months[is_leap(year)][month-1] + day-1 ) % 7;
}

int main(){
	fill_first_days(0);
	
	int cont = 0;
	for( int y=1901; y<=2000; ++y){
		for( int m=1; m<=12; ++m ){
			if( get_week_day(1,m,y) == 6 )
				cont++;
		}
	}
	cout << cont;
	return 0;
}