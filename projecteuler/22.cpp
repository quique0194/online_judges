#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;


template<class T>
void quicksort(vector<T> &list, int beg, int end ){
	if( beg >= end ) return;
	string p = list[(beg+end)/2];
	int i = beg;
	int j = end;
	while( i<=j ){
		while( list[i] < p ) ++i;
		while( list[j] > p ) --j;
		if( i<=j )
			swap( list[i++], list[j--] );
	}
	quicksort<T>(list, beg, j);
	quicksort<T>(list, i, end);
}

int get_score(string s){
	int sum=0;
	for(int i=0; i<s.size(); ++i)
		sum += s[i]-'A'+1;
	return sum;
}

int main(){

	vector<string> names;

	ifstream file("names.txt");
	while( !file.eof() ){
		string t;
		getline(file, t, ',');
		names.push_back(string(t.begin()+1, t.end()-1));
	}
	file.close();


	quicksort<string>(names,0,names.size()-1);

	int scores[names.size()];

	for(int i=0; i<names.size(); ++i){
		scores[i] = (i+1)*get_score(names[i]);
	}
		

	int ret = 0;
	for(int i=0; i<names.size(); ++i)
		ret += scores[i];

	cout << ret;

	return 0;
}


