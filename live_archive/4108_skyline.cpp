#include <vector>
#include <cmath>
using namespace std;

int overlap = 0;

// DT:data_type, data used to build the tree
// TT:tree_type, internal types of the nodes
// UT:update_type
template <class DT, class TT, class UT>
class SegmentTree{
public:
    // vector data is only used to build the tree
    // the tree doesn't update values in vector data
    SegmentTree(const vector<DT> &data): n(data.size()){
        tree.assign(pow(2, ceil( log2(n) ) + 1), 0);
        updates.assign(pow(2, ceil( log2(n) ) + 1), 0);
        build_tree(data,1,0,n-1);
    }

    // Query for the range [i..j] inclusive
    // Behaviour is defined in function merge
    TT query(int i, int j){
        return query(i, j, 1, 0, n-1);
    }

    // Updates the range [i..j] with the value u
    void update(int i, int j, const UT& u){
        update(i, j, u, 1, 0, n-1);
    }

private:
    int n;
    vector<TT> tree;
    vector<UT> updates;

    // OVERRIDE
    // Take two children nodes and merge them to get the parent node
    // Here is defined the behaviour of the tree
    TT merge(const TT& a, const TT& b){
        return (a>b)? a:b;
    }

    // OVERRIDE
    // Convert a data_type value to a tree_type value
    // Used in the leaves of the tree
    TT to_node(const DT& d){
        return d;
    }

    // OVERRIDE
    // 1. Update tree[v] with value u
    // Node tree[v] ranges from [L..R] inclusive
    // 2. Set updates[v] to be applied lazily to its descendients
    // Keep in mind that updates[v] may have a previous value
    void update_node(int v, int L, int R, const UT& u){
        if(updates[v]){
            update_node()
        }
        if(u>v){
            updates[v] += u;        
            updates[v] = u;
        }
    }

    void build_tree(const vector<DT> &data, int v, int L, int R){ // v:vertex
        if(L==R) tree[v] = to_node(data[L]);
        else{
            build_tree(data, 2*v, L, (L+R)/2);
            build_tree(data, 2*v+1, (L+R)/2 + 1, R);
            tree[v] = merge(tree[2*v], tree[2*v+1]);
        }
    }

    TT query(int i, int j, int v, int L, int R){
        if(L>=i && R<=j) return tree[v];
        int m = (L+R)/2;
        if(updates[v]){
            update_node(v*2, L, m, updates[v]);
            update_node(v*2+1, m+1, R, updates[v]);
            updates[v] = 0;
        }        
        if(j < m+1) return query(i, j, 2*v, L, m);
        if(i > m) return query(i, j, 2*v+1, m + 1, R);
        return merge(query(i, j, 2*v, L, m),
                     query(i, j, 2*v+1, m + 1, R));
    }
    
    void update(int i, int j, const UT& u, int v, int L, int R){
        if(L>=i && R<=j){ update_node(v, L, R, u); return;}
        int m = (L+R)/2;
        if(updates[v]){
            update_node(v*2, L, m, updates[v]);
            update_node(v*2+1, m+1, R, updates[v]);
            updates[v] = 0;
        }             
        if(i <= m) update(i, j, u, 2*v, L, m);
        if(j >= m+1) update(i, j, u, 2*v+1, m + 1, R);
        tree[v] = merge(tree[v*2], tree[v*2+1]);
    }
};

// ============================================================================

#include <iostream>

int main(){
    int c; cin >> c;
    while(c--){
        int n; cin >> n;
        SegmentTree<int, int, int> st(vector<int> v(n, 0));
        overlap = 0;
        while(n--){
            int l, r, h;
            cin >> l >> r >> h;
        }
    }
    return 0;
}