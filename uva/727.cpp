#include <iostream>
#include <string>
#include <stack>
#include <map>
using namespace std;

typedef stack<char> sc;
typedef map<char, int> ci;

int tc;
sc op; // op operators
string pf, line, discard; // pf postfix
ci precedence;

void set_precedence(){
    precedence['('] = 0;
    precedence['+'] = 1;
    precedence['-'] = 1;
    precedence['*'] = 2;
    precedence['/'] = 2;
}

int main(){
    set_precedence();
    cin >> tc;
    getline(cin, discard); getline(cin, discard);
    while(tc--){
        while(getline(cin, line)){
            if(line.empty()) break;
            char c = line[0];
            if(c=='(') op.push(c);
            else if(c==')'){ 
                while(op.top()!='('){
                    pf.push_back(op.top()); op.pop();
                }
                op.pop();            
            }
            else if(precedence.count(c)){
                while(!op.empty())
                    if(precedence[op.top()] >= precedence[c]){
                        pf.push_back(op.top()); op.pop();
                    } else break;
                op.push(c);
            }
            else pf.push_back(c);
        }
        while(!op.empty()){ pf.push_back(op.top()); op.pop();}
        cout << pf << endl;
        if(tc) cout << endl; 
        pf = "";
    }
    return 0;
}
