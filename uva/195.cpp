#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

int n;
string w;

bool isupper(const char c){
    return ('A'<=c && c<='Z');
}

char tolower(const char c){
    if(isupper(c)) return c + 'a' - 'A';
    return c;
}

// lt less than
bool lt(const char a, const char b){
    if(tolower(a) == tolower(b)) return isupper(a) && !isupper(b);
    return tolower(a) < tolower(b);
}

int main(){
    cin >> n;
    while(n--){
        cin >> w;
        sort(w.begin(), w.end(), lt);
        do{ cout << w << endl;
        }while(next_permutation(w.begin(), w.end(), lt));
    }
    return 0;
}
