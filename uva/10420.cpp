#include <iostream>
#include <string>
#include <map>
using namespace std;

typedef map<string, int> msi;
typedef msi::iterator msii;

int n;
msi countries;

int main(){
	cin >> n;
    for(int i=0; i<n; ++i){
        string country, discard;
        cin >> country;
        getline(cin, discard);
        if(countries.count(country))
            countries[country]++;
        else countries[country] = 1;
    }

    for(msii i=countries.begin(); i!=countries.end(); ++i)
            cout << i->first << " " << i->second << endl;
	return 0;
}
