#include <iostream>
#include <set>

using namespace std;

int N, M;
set<int> cds;

int main(){
    while(1){
        cin >> N >> M;
        if( N == 0 && M == 0 ) break;
        cds.clear();
        while(N--){
            int cd; cin >> cd;
            cds.insert(cd);
        }
        int cont = 0;
        while(M--){
            int cd; cin >> cd;
            if(cds.count(cd)) ++cont;
        }
        cout << cont << endl;
    }
    return 0;
}
