#include <cstdio>
#include <cstring> 
#include <algorithm>
#include <vector>

#define MAX 1000000

using namespace std;

bool is_p[MAX+1]; 
vector<int> primes;

void sieve(){
    memset(is_p, 1, sizeof is_p);
    for(int i=2; i*i<=MAX; ++i){
        if(is_p[i]){
            for(int j=i*i; j<=MAX; j+=i){
                is_p[j] = 0;
            }
        }  
    }
    for(int i=2; i<=MAX; ++i)
        if(is_p[i])  primes.push_back(i);
}

int main(){
    sieve();
    int n;
    printf("%d\n",primes.back());
    n = primes.back();
    int r=0;
    for(int i=0; 2*primes[i]<n; ++i){
        int t = upper_bound(primes.begin()+i, primes.end(), n-primes[i]) - 
            primes.begin();
        r += t - i - 1;
    }   
    printf("%d", r);
    return 0;
}
