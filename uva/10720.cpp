#include <iostream>
#include <algorithm>
using namespace std;

int n;
int degrees[10001];

bool erdos_gallai(){
    int sd = 0; // sum degrees
    for(int i=1; i<=n; ++i){
        if(degrees[i] < 0) return 0;
        sd += degrees[i];
    }
    if(sd&1) return 0;
    sort(degrees+1, degrees+n+1);
    reverse(degrees+1, degrees+n+1);
    for(int k=1; k<=n; ++k){
        int sd = 0; for(int i=1; i<=k; ++i) sd += degrees[i]; // sum degrees
        int sm = 0; for(int i=k+1; i<=n; ++i) sm += min(k,degrees[i]); // sum min
        if( sd > k*(k-1) + sm ) return 0;
    }
    return 1;
}

int main(){
    while(1){
        cin >> n;
        if(!n) break;
        for(int i=1; i<=n; ++i) cin >> degrees[i];
        if(erdos_gallai()) cout << "Possible" << endl;
        else cout << "Not possible" << endl;
    }
    return 0;
}
