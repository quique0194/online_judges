#include <iostream>
#include <algorithm>
using namespace std;

int N, B, H, W;
int p;

int main(){
    while(cin >> N >> B >> H >> W){
        int ans=1000000;
        while(H--){
            cin >> p;
            bool valid=false;
            int beds;
            for(int i=0; i<W; ++i){
                cin >> beds;
                if(beds>=N) valid=true;
            }
            if(valid) ans = min(ans,p*N);
        }
        if(ans<=B) cout << ans << endl;
        else cout << "stay home" << endl;
    }
    return 0;
}
