#include <cstdio>
#include <algorithm>
using namespace std;

int TC, N;

int main(){
    scanf("%d", &TC);
    for(int i=1; i<=TC; ++i){
        scanf("%d", &N);
        int ans=0;
        while(N--){
            int t; scanf("%d", &t);
            ans=max(ans, t);
        }
        printf("Case %d: %d\n", i, ans);
    }
    return 0;
}

