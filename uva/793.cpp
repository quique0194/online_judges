#include <iostream>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

int tc;
int n;
vector<int> pset;
int success, unsuccess;
string line;
string discard;

int findSet(int i){
    return (pset[i] == i)? i:pset[i]=findSet(pset[i]);
}

void unionSet(int i, int j){
    pset[findSet(i)] = findSet(j);
}

int main(){
    cin >> tc;
    while(tc--){
        cin >> n; getline(cin, discard);
        pset.assign(n+1, 0);
        for(int i=1; i<=n; ++i) pset[i] = i;
        success = 0; unsuccess = 0;
        while(getline(cin, line)){
            if(line.empty()) break;
            stringstream in(line);
            char type; int i, j;
            in >> type >> i >> j;
            if(type=='c') unionSet(i, j);
            else if( findSet(i) == findSet(j) ) success++;
            else unsuccess++;
        }
        cout << success << "," << unsuccess << endl;
        if(tc) cout << endl;
    }
    return 0;
}
