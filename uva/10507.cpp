#include <cstdio>
#include <map>
#include <vector>
#include <set>
using namespace std;

typedef vector<char> vc;
typedef map<char, vc> mcvc;

int n, m;
set<char> awake;
mcvc g; // graph

// returns years
int awake_all(){
    int y = 0;  // year
    bool c = 1; // changes
    while(c){
        c = 0;
        vc will_awake = vc();
        for(mcvc::iterator it = g.begin(); it != g.end(); ++it){
            if(awake.count(it->first)) continue;
            vc& adj = it->second;
            int ca = 0; // connections awake
            for( int i = 0; i < adj.size(); ++i)
                if(awake.count(adj[i])) ++ca;
            if(ca>=3){ will_awake.push_back(it->first); c=1;}
        }
        for(int i=0; i<will_awake.size(); ++i) awake.insert(will_awake[i]);
        y++;
    }
    return y-1;
}

int main(){
    while(scanf("%d\n%d\n", &n, &m)==2){
        g.clear();
        char c[3]; scanf("%c%c%c\n", &c[0], &c[1], &c[2]);
        awake = set<char>(c, c+3);
        while(m--){
            char a, b; scanf("%c%c\n", &a, &b);
            if( !g.count(a) ) g[a] = vc();
            if( !g.count(b) ) g[b] = vc();
            g[a].push_back(b); g[b].push_back(a);
        }
        int y = awake_all();
        if( awake.size() == n ) printf("WAKE UP IN, %d, YEARS\n", y);
        else printf("THIS BRAIN NEVER WAKES UP\n");
        
    }
    return 0;
}
