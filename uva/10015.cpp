#include <cstdio>
#include <vector>
#include <cstring>
using namespace std;

typedef vector<int> vi;


bool isp[1000000];
vi primes;
int n;

void sieve(){
    memset(isp, 1, sizeof isp);
    for(int i=2; i*i<1000000; ++i)
        if(isp[i])
            for(int j=i*i; j<1000000; j+=i)
                isp[j] = false;
    for(int i=2; i<1000000; ++i)
        if(isp[i]) primes.push_back(i);
}

// pp prime pos
int josephus(int n, int pp){
    if(n==1) return 0;
    return ( josephus(n-1, pp+1) + primes[pp] ) % n;
}

int main(){
    sieve();
    while(1){
        scanf("%d", &n);
        if(!n) break;
        printf("%d\n", josephus(n, 0)+1);
    }
    return 0;
}
   
