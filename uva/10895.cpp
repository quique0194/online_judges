#include <iostream>
#include <vector>
using namespace std;

typedef pair<int, int> ii;
typedef vector<ii> vii;

int m, n;
vector<vii> mat, tra; // matrix transpose
int pos[1001], num[1001];

int main(){
    while(cin >> m >> n){
        mat.assign(m, vii());
        tra.assign(n, vii());
        for(int i=0; i<m; ++i){
            int r; cin >> r;
            for(int j=0; j<r; ++j) cin >> pos[j];
            for(int j=0; j<r; ++j) cin >> num[j];
            for(int j=0; j<r; ++j) mat[i].push_back(ii(pos[j]-1, num[j]));
        }
        for(int i=0; i<mat.size(); ++i)
            for(int j=0; j<mat[i].size(); ++j){
                ii v = mat[i][j];
                tra[v.first].push_back(ii(i, v.second));
            }
        cout << n << " " << m << endl;
        for(int i=0; i<tra.size(); ++i){
            cout << tra[i].size();
            for(int j=0; j<tra[i].size(); ++j)
                cout << " " << tra[i][j].first+1;
            cout << endl;
            for(int j=0; j<tra[i].size(); ++j){
                if(j) cout << " ";
                cout << tra[i][j].second;
            }
            cout << endl;
        }
    }
    return 0;
}

