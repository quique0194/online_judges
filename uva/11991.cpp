#include <iostream>
#include <vector>

using namespace std;

typedef vector<int> vi;

int n, m;
vector<vi> oc; // ocurrences

int main(){
    while(cin >> n >> m){
        oc.assign(1000001, vi());
        for(int i=1; i<=n; ++i){
            int t; cin >> t;
            oc[t].push_back(i);
        }
        for(int i=0; i<m; ++i){
            int k, v;
            cin >> k >> v;
            if(k>oc[v].size()) cout << 0 << endl;
            else cout << oc[v][k-1] << endl;
        }
    }
    return 0;
}


