#include <iostream>
#include <vector>
#include <string>
using namespace std;

typedef vector<string> vs;

int tc;
vs hand, pile;

int get_value(string &card){
    if('2' <= card[0] && card[0] <= '9') return card[0]-'0';
    return 10;
}

int main(){
    cin >> tc;
    for(int t=1; t<=tc; ++t){
        hand.assign(25,""); pile.assign(27,"");
        for(int i=0; i<27; ++i) cin >> pile[i];
        for(int i=0; i<25; ++i) cin >> hand[i];
        int y=0;
        for(int i=0; i<3; ++i){
            int x = get_value(pile.back());
            y += x;
            for(int i=0; i<10-x+1; ++i) pile.pop_back();
        }
        cout << "Case " << t << ": ";
        if( y <= pile.size() ) cout << pile[y-1] << endl;
        else cout << hand[y-pile.size()-1] << endl;

    }
    
    return 0;
}
