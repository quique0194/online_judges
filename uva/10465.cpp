#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

typedef pair<int, int> ii;

int m, n, t;
vector<ii> memo;

ii best(const ii& a, const ii& b){
	if(a.second < b.second) return a;
	if(a.second==b.second)
		if(a.first > b.first) return a;
	return b;
}

// returns ii(burgers, beers)
ii eat_burgers(int t){
	if(memo[t]!=ii(0,0)) return memo[t];
	if(t<m && t<n) return ii(0, t);  
	ii ret;
	if(t<m) ret = eat_burgers(t-n);
	else if(t<n) ret = eat_burgers(t-m);
	else ret = best(eat_burgers(t-m), eat_burgers(t-n));
	ret.first++;
	return memo[t]=ret;
}

int main() {
	while(cin >> m >> n >> t){
		memo.assign(t+1,ii(0,0));
		ii ret = eat_burgers(t);
		cout << ret.first;
		if(ret.second) cout << " " << ret.second;
		cout << endl;
	}
	return 0;
}