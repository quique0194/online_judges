#include <iostream>
#include <map>
#include <string>
 using namespace std;

 map<string, string> pset; // parent set
 map<string, int> nset; // number of elements set

string findSet(string& x){
    return (pset[x]==x)? x:pset[x] = findSet(pset[x]);
}

bool isSameSet(string &x, string &y){
    return findSet(x) == findSet(y);
}

void unionSet(string& x, string& y){
    if(!isSameSet(x,y)) nset[findSet(y)] += nset[findSet(x)];
    pset[findSet(x)] = findSet(y); 
}

 int main(){
    int tc; cin >> tc;
    while(tc--){
        int F; cin >> F;
        pset.clear(); nset.clear();
        while(F--){
            string f1, f2;
            cin >> f1 >> f2;
            if(!pset.count(f1)){ pset[f1] = f1; nset[f1]=1;}
            if(!pset.count(f2)){ pset[f2] = f2; nset[f2]=1;}
            unionSet(f1, f2);
            cout << nset[findSet(f1)] << endl;
        }        
    }
    return 0;
 }
