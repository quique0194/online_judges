#include <iostream>
#include <map>
#include <algorithm>
#include <string>
using namespace std;

typedef map<string, int> msi;

int n;
msi p; // popularity
int mp; // max popularity

int main(){
    while(1){
        cin >> n;
        if(!n) break;
        p.clear();
        mp = 1;
        string courses[5];
        for(int i=0; i<n; ++i){
            for(int j=0; j<5; ++j) cin >> courses[j];
            sort(courses, courses+5);
            string code;
            for(int j=0; j<5; ++j) code += courses[j]; 
            if(p.count(code)){
                if(++p[code]>mp) mp = p[code];
            }
            else p[code] = 1;
        }
        int cont = 0;
        for(msi::iterator it = p.begin(); it!=p.end(); ++it)
            if( it->second == mp ) cont += it->second;
        cout << cont << endl;
    }
    return 0;
}

