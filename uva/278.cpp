#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;

int tc, m, n;
char piece;

int main(){
    cin >> tc;
    while(tc--){
        cin >> piece >> m >> n;
        if(piece=='r' || piece=='Q') cout << min(m,n) << endl;
        else if(piece=='k') cout << ceil(m*n/2.0) << endl;
        else if(piece=='K') cout << ceil(m/2.0)*ceil(n/2.0) << endl;
    }
    return 0;
}
