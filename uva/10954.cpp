#include <cstdio>
#include <queue>
#include <vector>
#include <functional>
using namespace std;

typedef priority_queue< int, vector<int>, greater<int> > pqi;

int n, cost;
pqi h; // h heap

int main(){
    while(1){
        scanf("%d",&n);
        if(!n) break;
        while(n--){
            int t; scanf("%d",&t);
            h.push(t);
        }
        cost = 0;
        while(h.size()>1){
            int a = h.top(); h.pop();
            int b = h.top(); h.pop();
            int s = a + b; cost += s;
            h.push(s);
        }
        h.pop();
        printf("%d\n",cost);
    }
    return 0;
}

