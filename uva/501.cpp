#include <iostream>
#include <set>
#include <string>
using namespace std;

typedef multiset<int> ms;
typedef ms::iterator msit;

int k;
int m, n; 
int a[30002], u[30002];
string discard;
ms bb; // black box
msit imin; // i minimum

int main(){
    cin >> k; getline(cin, discard);
    while(k--){
        getline(cin, discard);
        cin >> m >> n;
        for(int i=1; i<=m; ++i) cin >> a[i];
        for(int i=1; i<=n; ++i) cin >> u[i];
        getline(cin, discard);
        int ui=1;
        bb.clear(); imin = bb.begin();
        for(int i=1; i<=m; ++i){
            bb.insert(a[i]);
            if( a[i] < *imin || imin == bb.end() ) imin--;
            if( ui > n ) break;
            while( u[ui] == i ){
                cout << *imin << endl;
                ui++; imin++;
            }
        }
        if(k) cout << endl;
    }
    return 0;
}
