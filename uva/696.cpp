#include <iostream>
#include <cmath>
using namespace std;

int m, n;

int max_knights(int m, int n){ // m<n
    if(m==1) return n;
    if(m==2) return 4*(n/4) + ( (n%4==3)? 4:2*(n%4) );
    return ceil(m*n/2.0);
}
int main(){
    while(1){
        cin >> m >> n;
        if(m==0) break;
        int ans = (m<n)? max_knights(m,n):max_knights(n,m);
        cout << ans << " knights may be placed on a " << m << " row " << 
            n << " column board." << endl;
    }
    return 0;
}
