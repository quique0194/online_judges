#include <vector>
#include <cmath>
using namespace std;

template <class T>
class SegmentTree{
    typedef bool (*choose_function)(const T& a, const T& b); // 0 first 1 second
public:
    SegmentTree(vector<T> &_data, choose_function _choose): 
        data(_data), choose(_choose)
    {
        tree.assign(pow(2, ceil( log2(data.size()) ) + 1), 0);
        build_tree(1,0,data.size()-1);
    }

    int querypos(int i, int j){
        return querypos(i, j, 1, 0, data.size()-1);
    }

    T query(int i, int j){
        int res = querypos(i, j, 1, 0, data.size()-1);
        return data[res];
    }

    // comon choose functions
    static bool minimum(const T &a, const T &b){ return b<a; }
    static bool maximum(const T &a, const T &b){ return b>a; }

private:
    vector<T> data;
    vector<T> tree;
    choose_function choose;

    void build_tree(int vertex, int L, int R){
        if(L==R) tree[vertex] = L;
        else{
            build_tree(2*vertex, L, (L+R)/2);
            build_tree(2*vertex+1, (L+R)/2 + 1, R);
            T l = data[tree[2*vertex]]; T r = data[tree[2*vertex+1]];
            tree[vertex] = tree[2*vertex+choose(l, r)];
        } 
    }

    int querypos(int i, int j, int vertex, int L, int R){
       if(j<L || i>R) return -1;
       if(L>=i && R<=j) return tree[vertex];
       int p1 = querypos(i, j, 2*vertex, L, (L+R)/2);
       int p2 = querypos(i, j, 2*vertex+1, (L+R)/2 + 1, R);
       if(p1 == -1) return p2;
       if(p2 == -1) return p1;
       if(choose(data[p1], data[p2])) return p2;
       else return p1;
    }

    
};
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

typedef vector<int> vi;
typedef SegmentTree<int> sti;

int n, q;

int count(int i, const vi& v){
    int cont = 1;
    for(int j=i+1; j<n && v[j]==v[i]; ++j)
        cont++;
    return cont;
}

int main(){
    while(scanf("%d %d", &n, &q)==2){
        vi a(n, 0);
        vi rpt(n, 0);
        vi start(n, 0);
        
        for(int i=0; i<n; ++i)
            scanf("%d", &a[i]);
        for(int i=1; i<n; ++i)
            start[i] = (a[i]==a[i-1])? start[i-1]:i;
        rpt[0] = count(0, a);
        for(int i=1; i<n; ++i)
            rpt[i] = (a[i]==a[i-1])? rpt[i-1]:count(i, a);

        sti st(rpt, sti::maximum);
        while(q--){
            int i, j;
            scanf("%d %d", &i, &j);
            --i; --j;
            if(a[i]==a[j]) printf("%d\n", j-i+1);
            else{
                int x = rpt[i] - (i-start[i]);
                int beg = start[i]+rpt[i]; int end = start[j]-1;
                int y = (end-beg>=0)? st.query(beg,end):0;
                int z = j - start[j] + 1;
                printf("%d\n", max(x,max(y,z)));
            }
        }
    }
    return 0;
}
