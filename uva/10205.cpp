#include <iostream>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

typedef vector<int> vi;

string suits[] = {"Clubs","Diamonds","Hearts","Spades"};
string values[] = {"2","3","4","5","6","7","8","9","10","Jack","Queen","King",
                   "Ace"};
string cards[53];
int tc, n, k;
int shuffles[101][53];
vi order;

void set_card_names(){
    for(int i=0;i<4;++i)
        for(int j=0;j<13;++j)
            cards[i*13+j+1] = values[j] + " of " + suits[i];
}

void init_card_position(){
    order.assign(53,0);
    for(int i=1;i<=52;++i)
        order[i] = i;
}

int main(){
    set_card_names();
    cin >> tc;
    while(tc--){
        cin >> n;
        for(int i=1; i<=n; ++i)
            for(int j=1;j<=52;++j)
                cin >> shuffles[i][j];
        init_card_position();
        string discard; getline(cin,discard);
        while(1){
            string line; getline(cin,line);
            if(line.empty()) break;
            stringstream in(line);
            in >> k;
            vi new_order; new_order.assign(53,0);
            for(int i=1;i<=52;++i)
                new_order[i] = order[shuffles[k][i]];
            order = new_order;
        }
        for(int i=1;i<=52;++i)
            cout << cards[order[i]] << endl;
        if(tc) cout << endl;
    }
    return 0;
}
