#include <vector>
#include <cmath>
using namespace std;

template <class DT, class TT, class UT> // DT:data_type TT:tree_type UT:update_type
class SegmentTree{
public:
    SegmentTree(const vector<DT> &data): n(data.size()){
        tree.assign(pow(2, ceil( log2(n) ) + 1), 0);
        updates.assign(pow(2, ceil( log2(n) ) + 1), 0);
        build_tree(data,1,0,n-1); }

    TT query(int i, int j){ return query(i, j, 1, 0, n-1); }
    void update(int i, int j, const UT& uval){ update(i, j, uval, 1, 0, n-1);}

private:
    int n;
    vector<TT> tree;
    vector<UT> updates;

    TT merge(const TT& a, const TT& b){ return a+b;}
    TT to_node(const DT& d){ return d=='1';}
    void update_node(int v, int L, int R, const UT& u){
        if(u=='F'){ tree[v] = R-L+1; updates[v] = 'F';}
        else if(u=='E'){ tree[v] = 0; updates[v] = 'E';}
        else if(u=='I'){
            tree[v] = R-L+1-tree[v];
            if(updates[v]){
                if(updates[v] == 'E') updates[v] = 'F';
                else if(updates[v] == 'F') updates[v] = 'E';
                else if(updates[v] == 'I') updates[v] = 0;
            }
            else
                updates[v] = 'I';        
        }
        
    }

    void build_tree(const vector<DT> &data, int v, int L, int R){ // v:vertex
        if(L==R) tree[v] = to_node(data[L]);
        else{
            build_tree(data, 2*v, L, (L+R)/2);
            build_tree(data, 2*v+1, (L+R)/2 + 1, R);
            tree[v] = merge(tree[2*v], tree[2*v+1]);
        }
    }

    TT query(int i, int j, int v, int L, int R){
        if(L>=i && R<=j) return tree[v];
        int m = (L+R)/2;
        if(updates[v]){
            update_node(v*2, L, m, updates[v]);
            update_node(v*2+1, m+1, R, updates[v]);
            updates[v] = 0;
        }        
        if(j < m+1) return query(i, j, 2*v, L, m);
        if(i > m) return query(i, j, 2*v+1, m + 1, R);
        return merge(query(i, j, 2*v, L, m),
                     query(i, j, 2*v+1, m + 1, R));
    }
    
    void update(int i, int j, const UT& uval, int v, int L, int R){ // uval:update_val
        if(L>=i && R<=j){ update_node(v, L, R, uval); return;}   
        int m = (L+R)/2;
        if(updates[v]){
            update_node(v*2, L, m, updates[v]);
            update_node(v*2+1, m+1, R, updates[v]);
            updates[v] = 0;
        }             
        if(i <= m) update(i, j, uval, 2*v, L, m);
        if(j >= m+1) update(i, j, uval, 2*v+1, m + 1, R);
        tree[v] = merge(tree[v*2], tree[v*2+1]);
    }
};
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

int TC, M, T, Q;
string pirates;

int main(){
    scanf("%d", &TC);
    for(int tc=1; tc<=TC; ++tc){
        scanf("%d", &M);
        pirates.clear();
        while(M--){
            scanf("%d", &T);
            char buffer[51];
            scanf("%s\n", buffer);
            while(T--) pirates.append(buffer);
        }
        vector<char> t = vector<char>(pirates.begin(), pirates.end());
        SegmentTree<char, int, char> st(t);
        printf("Case %d:\n", tc);
        scanf("%d\n", &Q); int qi = 1;
        while(Q--){
            char q; int a, b;
            scanf("%c %d %d\n", &q, &a, &b);
            if(q=='S') printf("Q%d: %d\n", qi++, st.query(a,b));
            else st.update(a,b,q);
        }
    }
    return 0;
}
