#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <cstring>
using namespace std;

char suites[] = {'S','H','D','C'};
char ranks[] = {' ','A','2','3','4','5','6','7','8','9','T','J','Q','K'};

int hand[4][14];

int add_to_hand(string &s){
    int rank = distance(ranks,find(ranks,ranks+13,s[0]));
    int suit = distance(suites,find(suites,suites+4,s[1]));
    hand[suit][rank]=1;
}

void sum_suits(){
    for(int i=0; i<4; ++i)
        for(int j=1; j<=13; ++j)
            if(hand[i][j]) hand[i][0]++;
}

bool all_suits_stopped(){
    for(int i=0;i<4;i++){
        if( !hand[i][1] && 
            !(hand[i][13] && hand[i][0]>=2) && 
            !(hand[i][12] && hand[i][0]>=3) )
                    return false;
    }
    return true;
}

int main(){
    string line;
    while(getline(cin,line)){
        stringstream cards(line);
        string card;
        memset(hand,0,sizeof hand);
        while(cards>>card){
            add_to_hand(card);
        }
        sum_suits();
        int points=0;
        int ntpoints=0;
        for(int i=0;i<4;i++){
            int t=0;
            if(hand[i][1]) t+=4;
            if(hand[i][13]) t+=3-(hand[i][0]==1);
            if(hand[i][12]) t+=2-(hand[i][0]<=2);
            if(hand[i][11]) t+=1-(hand[i][0]<=3);
            ntpoints+=t;
            t += (hand[i][0]==2) + 2*(hand[i][0]==1) + 2*(!hand[i][0]);
            points+=t;
        }
        if(ntpoints>=16 && all_suits_stopped()) cout << "BID NO-TRUMP" << endl;
        else if(points>=14){
            int maxsuit=0;
            for(int i=1;i<4;++i)
                if(hand[maxsuit][0]<hand[i][0]) maxsuit=i;
                cout << "BID " << suites[maxsuit] << endl;
        }
        else cout << "PASS" << endl;
    }
    return 0;
}
