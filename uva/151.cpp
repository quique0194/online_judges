#include <cstdio>
#include <cstring>

int n;

// lc last city
int lc(int n, int k){
    if(n==1) return 0;
    return ( lc(n-1, k) + k ) % n;
}

int main(){
    while(1){
        scanf("%d",&n);
        if(!n) break;
        n--;
        int k=1; 
        while(1){
            if(lc(n,k)==11) break;
            k++;
        }
        printf("%d\n", k);
    }
    return 0;
}
