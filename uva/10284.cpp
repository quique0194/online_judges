#include <iostream>
#include <string>
#include <cstring>
using namespace std;

char board[8][8];

bool occupied(int r, int c){
    return board[r][c] && board[r][c] != 'a';
}


bool attack(int r, int c){
    if( r<0 || r>=8 || c<0 || c>=8) return false;
    if(occupied(r,c)) return false;
    board[r][c] = 'a';
    return true;
}

int main(){
    string line;
    while(getline(cin,line)){
        memset(board,0, sizeof board);
        int r=0, c=0;
        for(int i=0; i<line.size(); ++i){
            if( '1' <= line[i] && line[i] <= '8' ) c += line[i] - '0';
            else if( line[i] == '/' ){ r++; c=0;}
            else board[r][c++] = line[i];
        }
        for(int r=0; r<8; ++r) for(int c=0; c<8; ++c) if(occupied(r,c)){
            if(board[r][c]=='p'){ attack(r+1,c+1); attack(r+1,c-1);}
            if(board[r][c]=='P'){ attack(r-1,c+1); attack(r-1,c-1);}
            if(board[r][c]=='r' || board[r][c]=='R' || 
                    board[r][c]=='q' || board[r][c]=='Q'){
                int i=c+1; while(attack(r,i++)); 
                i=c-1; while(attack(r,i--));
                i=r+1; while(attack(i++,c));
                i=r-1; while(attack(i--,c));
            }
            if(board[r][c]=='b' || board[r][c]=='B'||
                    board[r][c]=='q' || board[r][c]=='Q' ){
                int i=r+1, j=c+1; while(attack(i++,j++));
                i=r-1; j=c+1; while(attack(i--,j++));
                i=r+1; j=c-1; while(attack(i++,j--));
                i=r-1; j=c-1; while(attack(i--,j--));
            }
            if(board[r][c]=='k' || board[r][c]=='K')
                for(int i=r-1; i<=r+1; ++i) for(int j=c-1; j<=c+1; ++j)
                    attack(i,j);
            if(board[r][c]=='n' || board[r][c]=='N'){
                attack(r+2,c-1); attack(r+2,c+1);
                attack(r+1,c-2); attack(r-1,c-2);
                attack(r-2,c-1); attack(r-2,c+1);
                attack(r+1,c+2); attack(r-1,c+2);
            }
        }
        int cont = 0;
        for(int r=0; r<8; ++r) for(int c=0; c<8; ++c) if(!board[r][c]) cont++;
        cout << cont << endl;        
    }
    return 0;
}
