#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

int M, C, price[21][21];

int memo[201][21];

int shop(int money, int g){
    if(money < 0) return -1000000;
    if(g==C) return M - money;
    if(memo[money][g]!=-1)
        return memo[money][g];
    int ans=-1000000;
    for(int i=1; i<=price[g][0]; ++i)
        ans = max(ans, shop(money-price[g][i], g+1));        
    return memo[money][g]=ans;
}

int main(){
    int N;
    scanf("%d", &N);
    while(N--){
        scanf("%d %d", &M, &C);
        memset(memo, -1, sizeof memo);
        for(int i=0; i<C; ++i){
            scanf("%d", &price[i][0]);
            for(int j=1; j<=price[i][0]; ++j){
                scanf("%d", &price[i][j]);
            }
        }
        int ans = shop(M,0);
        if(ans<0) printf("no solution\n");
        else printf("%d\n",shop(M,0));
    }
    return 0;
}

