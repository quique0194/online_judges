#include <iostream>
#include <string.h>
#include <algorithm>
using namespace std;

#define CONT_SIZE 5

int cont[CONT_SIZE];

int main(){
	int n;
	cin >> n;

	int s[n];
	for(int i=0; i<n; ++i)
		cin >> s[i];

	
	memset(cont, 0, sizeof cont);
	for(int i=0; i<n; ++i)
		cont[s[i]]++;

	// strategy
	cont[1] = max(0, cont[1]-cont[3]);
	cont[1] += 2*( cont[2]%2 != 0 );
	cont[2] >>= 1;
	cont[1] = cont[1]/4 + (cont[1]%4 != 0);

	int sum = 0;
	for(int i=1; i<CONT_SIZE; ++i)
		sum += cont[i];

	cout << sum;
	return 0;
}