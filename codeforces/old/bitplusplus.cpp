#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;
	
	int count = 0;
	char statement[n][3];
	for(int i=0; i<n; ++i){
		cin >> statement[i];
	}

	for(int i=0; i<n; ++i){
		if( statement[i][1] == '+' )
			++count;
		else
			--count;
	}

	cout << count;
	return 0;
}