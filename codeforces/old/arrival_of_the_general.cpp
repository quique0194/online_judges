#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;

	int soldier[n];
	for(int i=0; i<n; ++i)
		cin >> soldier[i];

	int M = 0;
	int m = 0;
	for(int i=1; i<n; ++i){
		if( soldier[i] > soldier[M] )
			M = i;
		else if( soldier[i] <= soldier[m] )
			m = i;
	}

	cout << (n-m-1) + M - (m<M);

	return 0;
}