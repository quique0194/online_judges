#include <iostream>

using namespace std;

bool is_beautiful(int year){
	int aux[10] = {0};
	while(year){
		int c = year%10;
		if(aux[c]) return 0;
		aux[c] = 1;
		year /= 10;
	}
	return 1;
}

int main(){
	int y;
	cin >> y;

	++y;

	while(!is_beautiful(y)) ++y;
	cout << y;
	return 0;
}