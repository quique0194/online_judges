#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;
	
	char colors[n];
	cin >> colors;

	int count = 0;
	char prev = colors[0];
	for(int i=1; i<n; ++i){
		if( colors[i]==prev )
			count++;
		prev = colors[i];
	}

	cout << count;
	return 0;
}