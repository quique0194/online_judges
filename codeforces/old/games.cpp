#include <cstdio>
#include <utility>
using namespace std;

typedef pair<int, int> ii;

int n;
ii uniforms[30];

int main(){
    scanf("%d",&n);
    for(int i=0; i<n; ++i)
        scanf("%d %d", &uniforms[i].first, &uniforms[i].second);
    int cont=0;
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            cont += uniforms[i].first == uniforms[j].second;
    printf("%d",cont);
    return 0;
}

