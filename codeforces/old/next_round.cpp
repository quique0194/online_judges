#include <iostream>

using namespace std;

int main(){
	int n, k;
	cin >> n >> k;

	int scores[n];
	for(int i=0; i<n; ++i){
		cin >> scores[i];
	}

	int limit = scores[k-1];
	int i;
	if(limit)
		for(i=k-1; scores[i]==limit; ++i);
	else
		for(i=0; scores[i]>0; ++i);

	cout << i;
	return 0;
}