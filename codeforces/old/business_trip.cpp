#include <iostream>
#include <algorithm>
using namespace std;

int k, a[12];

int main(){
    cin >> k;
    for(int i=0; i<12; ++i)
        cin >> a[i];
    sort(a,a+12);
    reverse(a,a+12);
    int i, sum=0;
    for(i=0; i<12 && sum<k; ++i)
        sum+=a[i];
    cout << ( (sum>=k)? i : -1 );
    return 0;
}
