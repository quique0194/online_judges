#include <cstdio>

int n;
int lucky_numbers[] = {4,7,44,77,47,74,444,447,477,777,774,477,474,747};

int main(){
    scanf("%d", &n);
    for(int i=0; i<14; ++i)
        if(n%lucky_numbers[i] == 0){
            printf("%s","YES");
            return 0;
        }
    printf("%s","NO");
    return 0;
}
