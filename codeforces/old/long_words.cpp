#include <iostream>
#include <string>
using namespace std;

string tostr(int x){
	string toret;
	while(x){
		toret += x%10 +'0';
		x/=10;
	}
	return string(toret.rbegin(), toret.rend());
}

int main(){
	int n;
	cin >> n;

	string words[n];
	for(int i=0; i<n; ++i)
		cin >> words[i];

	for(int i=0; i<n; ++i){
		if(words[i].size()>10){
			words[i] = words[i][0] + tostr(words[i].size()-2) + words[i][words[i].size()-1];
		}
	}

	for(int i=0; i<n; ++i)
		cout << words[i] << endl;
	return 0;
}