#include <iostream>

using namespace std;

int gcd(int a, int b){
	if( a == 0 )
		return b;
	return gcd(b%a, a);
}

int main(){
	
	int players[2];
	int n;
	cin >> players[0] >> players[1] >> n;

	int count = 0;
	while( n>0 ){
		n -= gcd(players[count&1], n);
		++count;
	}

	cout << (--count&1);

	return 0;
}