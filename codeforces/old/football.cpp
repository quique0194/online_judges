#include <iostream>
#include <string>

using namespace std;

#define DANGER 7

int main(){
	string s;
	cin >> s;

	char prev = s[0];
	int counter = 1;
	for(int i=1; i<s.size() && counter < DANGER; ++i){
		if( s[i] == prev )
			++counter;
		else
			counter = 1;
		prev = s[i];
	}

	if( counter == DANGER )
		cout << "YES";
	else
		cout << "NO";
	
	return 0;
}