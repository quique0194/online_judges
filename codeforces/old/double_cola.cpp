#include <iostream>
#include <string>
using namespace std;

#define N_FRIENDS 5

string friends[N_FRIENDS]={
	"Sheldon",
	"Leonard",
	"Penny",
	"Rajesh",
	"Howard"
};

int pow(int b, int e){
	int r = 1;
	while(e){
		r*=b;
		--e;
	}
	return r;
}

int main(){
	int n;
	cin >> n;
	--n;

	int e=0;
	while( N_FRIENDS*pow(2,e) <= n ){
		n -= N_FRIENDS*pow(2,e++);
	}

	cout << friends[n/pow(2,e)];
	return 0;
}