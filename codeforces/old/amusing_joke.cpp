#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

string a, b, c;

int main(){
    cin >> a >> b >> c;
    string t = a+b;
    sort(t.begin(), t.end());
    sort(c.begin(), c.end());
    if( t == c ) cout << "YES";
    else cout << "NO";
    return 0;
}
