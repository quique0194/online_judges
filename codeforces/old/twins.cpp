#include <iostream>
#include <algorithm>
using namespace std;

void quicksort(int* a, int beg, int end){
	if( beg >= end )
		return;
	int p = a[(beg+end)/2];
	int i = beg;
	int j = end;
	while( i <= j ){
		while(a[i] < p) ++i;
		while(a[j] > p) --j;
		if( i <= j )
			swap(a[i++], a[j--]);
	}
	quicksort(a, beg, j);
	quicksort(a, i, end);
}

int main(){

	int n;
	cin >> n;

	int coin[n];
	for(int i=0; i<n; ++i)
		cin >> coin[i];

	int total=0;
	for(int i=0; i<n; ++i)
		total += coin[i];

	quicksort( coin, 0, n-1 );

	int i = n-1;
	int sum = 0;
	while( 2*sum <= total )
		sum += coin[i--];

	cout << n-i-1;

	return 0;
}