#include<iostream>
#include<string.h>
using namespace std;

int mem[1000001];
int xor_sum(int n){
    if( n%2 == 1 )
        return (n % 4 == 1) ? 1 : 0;
    else
        return (n % 4 == 0) ? n : n + 1;
}

int xor_sum_mod(int n, int m){
    int ret = 0;
    if( (n/m) & 1 ){
        ret ^= xor_sum(m-1);
    }
    return ret ^ xor_sum(n%m);
}
int main(){
    memset(mem, -1, sizeof mem);
    int n; cin >> n;
    int Q = 0;
    for(int i=1; i<=n; ++i){
        int pi; cin >> pi;
        Q ^= pi;
        Q ^= xor_sum_mod(n, i);
    }    
    cout << Q << endl;
    return 0;
}
