#include <iostream>
#include <string>
using namespace std;


char vocals[] = "aeiouy";

bool is_vocal(char c){
	int i=0;
	while(vocals[i]){
		if(c==vocals[i++])
			return 1;
	}
	return 0;
}

int main(){
	string s;
	cin >> s;

	// tolower
	for(int i=0; i<s.size(); ++i)
		if( 'A' <= s[i] && s[i] <= 'Z' )
			s[i] += 'a' - 'A';

	// delete vocals and add points
	string r;	
	for(int i=0; i<s.size(); ++i)
		if(!is_vocal(s[i]))
			r += string(".") + s[i]; 

	cout << r;

	return 0;
}