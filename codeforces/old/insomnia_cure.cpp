#include <iostream>
#include <cstring>
using namespace std;

#define MAX_ATTACKS 4

int main(){
	int attack[MAX_ATTACKS];
	for( int i=0; i<MAX_ATTACKS; ++i )
		cin >> attack[i];

	int d;
	cin >> d;

	bool is_damaged[d+1];
	memset(is_damaged,0,sizeof is_damaged);
	for( int i=0; i<MAX_ATTACKS; ++i )
		if( !is_damaged[attack[i]] )
			for( int j=attack[i]; j<=d; j+=attack[i] )
				is_damaged[j] = 1;

	int count = 0;
	for( int i=1; i<=d; ++i)
		if( is_damaged[i] )
			++count;

	cout << count;

	return 0;
}