#include <iostream>

using namespace std;

int main(){
	int n, m, a;
	cin >> n >> m >> a;

	long long int h = n/a;
	long long int v = m/a;
	if( n%a ) ++h;
	if( m%a ) ++v;

	cout << h*v;

	return 0;
}