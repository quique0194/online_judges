#include <iostream>
#include <string>
using namespace std;

string word;
string target="hello";

int main(){
    cin >> word;
    int i=0, j=0;
    for(int i=0; i<word.size(); ++i)
        if(word[i]==target[j] && j<target.size()) 
            j++;
    cout << ( (j==target.size())? "YES":"NO" ) << endl;
    return 0;
}
