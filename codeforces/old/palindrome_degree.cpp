// http://codeforces.com/problemset/problem/7/D

#include <iostream>
using namespace std;

#define X 91 

typedef unsigned int uint;

uint power[1000001] = {0}; 
char s[1000001];
uint hash[1000001];

uint x_power(int e){
    if(power[e]) return power[e];
    return power[e] = x_power(e-1) * X;
}

int main(){
    power[0] = 1;
    cin >> s;
    hash[0] = (s[0]-'a');
    for(int i=1; s[i]; ++i){
        hash[i] = hash[i-1] + (s[i]-'a') * x_power(i);
        cout << i << " HASH: " << hash[i] << endl;
    }
    while(1){ 
        int i,j,k,l; 
        cin >> i >> j>>k>>l; 
        cout << x_power(i) << " " << x_power(k) << endl;
        cout << (hash[j]-hash[i-1])*x_power(k-i) << endl;
        cout << (hash[l]-hash[k-1]) << endl;
    }
    return 0;
}
