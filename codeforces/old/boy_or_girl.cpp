#include <iostream>
#include <string>

using namespace std;

void quicksort(string& a, int beg, int end){
	if( beg >= end )
		return;
	int p = a[(beg+end)/2];
	int i = beg;
	int j = end;
	while( i <= j ){
		while(a[i] < p) ++i;
		while(a[j] > p) --j;
		if( i <= j )
			swap(a[i++], a[j--]);
	}
	quicksort(a, beg, j);
	quicksort(a, i, end);
}

int main(){
	string w;
	cin >> w;

	quicksort(w, 0, w.size()-1);

	int count = 1;
	for( int i=1; i<w.size(); ++i )
		count += w[i] != w[i-1];

	if( count&1 )
		cout << "IGNORE HIM!";
	else
		cout << "CHAT WITH HER!";
	return 0;
}