#include <iostream>
#include <string>
using namespace std;

bool is_lower(char c){
	return 'a' <= c && c <= 'z';
}

bool need_change(string& s){
	for(int i=1; i<s.size(); ++i){
		if(is_lower(s[i]))
			return 0;
	}
	return 1;
}

string& change_case(string& s){
	for(int i=0; i<s.size(); ++i){
		if(is_lower(s[i]))
			s[i] += 'A' - 'a';
		else
			s[i] += 'a' - 'A';
	}
	return s;
}

int main(){
	string w;
	cin >> w;
	if( need_change(w) )
		change_case(w);
	cout << w;
	return 0;
}
