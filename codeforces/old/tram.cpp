#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;
	int p[n][2];
	for(int i=0; i<n; ++i)
		cin >> p[i][0] >> p[i][1];

	int max = 0;
	int total=0;
	for(int i=0; i<n; ++i){
		total = total - p[i][0] + p[i][1];
		if(total>max)
			max=total;
	}

	cout << max;

	return 0;
}