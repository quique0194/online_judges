#include <iostream>

using namespace std;

#define N 4

void quicksort(int* a, int beg, int end){
	if( beg >= end )
		return;
	int p = a[(beg+end)/2];
	int i = beg;
	int j = end;
	while( i <= j ){
		while(a[i] < p) ++i;
		while(a[j] > p) --j;
		if( i <= j )
			swap(a[i++], a[j--]);
	}
	quicksort(a, beg, j);
	quicksort(a, i, end);
}

int main(){
	int horseshoe[N];
	for(int i=0; i<N; ++i)
		cin >> horseshoe[i];

	quicksort( horseshoe, 0, N-1 );

	int count=0;
	for(int i=1; i<N; ++i)
		count += horseshoe[i] == horseshoe[i-1];

	cout << count;
	return 0;
}