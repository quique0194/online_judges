#include <iostream>

using namespace std;

int main(){
	int n; 
	cin >> n;
	int s[n][3];
	for(int i=0; i<n; ++i)
		cin >> s[i][0] >> s[i][1] >> s[i][2];

	int r[n];
	for(int i=0; i<n; ++i)
		r[i] = s[i][0] + s[i][1] + s[i][2];

	int cont=0;
	for(int i=0; i<n; ++i){
		if(r[i]>1)
			++cont;
	}
		

	cout << cont;
	return 0;
}