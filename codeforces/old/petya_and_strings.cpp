#include <iostream>
#include <string>
using namespace std;

string& to_lower(string& s){
	for(int i=0; i<s.size(); ++i)
		if( 'A' <= s[i] && s[i] <= 'Z')
			s[i] += 'a' - 'A';
	return s;
}

int main(){
	string a, b;
	cin >> a >> b;

	to_lower(a);
	to_lower(b);

	if( a < b )
		cout << -1;
	else if( a == b )
		cout << 0;
	else cout << 1;

	return 0;
}