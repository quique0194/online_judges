#include <iostream>
#include <queue>

using namespace std;

int n,m,k,p;

int a[1000][1000];

int main(){
    cin >> n >> m >> k >> p;
    priority_queue<int> rows, cols;
    for(int i=0; i<n; ++i){
        for(int j=0; j<m; ++j){
            cin >> a[i][j];
        }
    }

    for(int i=0; i<n; ++i){
        int row = 0;
        for(int j=0; j<m; ++j){
            row += a[i][j];
        }
        rows.push(row);
    }

    for(int i=0; i<m; ++i){
        int col = 0;
        for(int j=0; j<n; ++j){
            col += a[j][i];
        }
        cols.push(col);
    }

    long long pleasure = 0;
    int row_penalization = 0;
    int col_penalization = 0;
    while(k--){
        if(rows.top()-row_penalization > cols.top()-col_penalization){
            pleasure += rows.top() - row_penalization;
            int t = rows.top() - m*p;
            rows.pop();
            rows.push(t);
            col_penalization += p;
        }
        else{
            pleasure += cols.top() - col_penalization;
            int t = cols.top() - n*p;
            cols.pop();
            cols.push(t);
            row_penalization += p;
        }
    }

    cout << pleasure << endl;
    return 0;
}
