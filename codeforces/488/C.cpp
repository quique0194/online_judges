#include <iostream>
using namespace std;

int hm, am, dm, hy, ay, dy, h, a, d;

int survive_turns (int h, int damage) {
    if (h%damage == 0) {
        return h/damage - 1;
    } else {
        return h/damage;
    }
}

bool win(int hy, int ay, int dy) {
    int y_damage = am - dy,
    m_damage = ay - dm;
    if (m_damage <= 0) {
        return false;
    }
    if (y_damage <= 0) {
        return true;
    }
    return survive_turns(hy, y_damage) > survive_turns(hm, m_damage);
}

int mini = 0x7fffffff;

int main () {
    cin >> hy >> ay >> dy;
    cin >> hm >> am >> dm;
    cin >> h >> a >> d;
    for (int i = 0; i <= 1000; ++i) {
        for (int j = 0; j <= 200; ++j) {
            for (int k = 0; k <= 100; ++k) {
                if (win(hy+i, ay+j, dy+k)) {
                    int coins = i*h + j*a + k*d;
                    if (coins < mini) {
                        mini = coins;
                    }
                }
            }
        }
    }
    cout << mini << endl;
    return 0;
}