#include <iostream>
using namespace std;

typedef long long ll;

bool lucky(ll x) {
    if (x<0) {
        x = -x;
    }
    while (x) {
        if (x%10 == 8) {
            return true;
        }
        x/=10;
    }
    return false;
}

int main() {
    int a;
    cin >> a;
    int cont = 1;
    while (!lucky(a+cont)) {
        cont++;
    }
    cout << cont;
    return 0;
}