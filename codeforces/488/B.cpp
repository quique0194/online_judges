#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

int n;
int a[4];
int b[4];

bool check() {
    // doesn't change a
    memcpy(b, a, sizeof(a));
    sort(b, b+4);
    int range = b[3] - b[0];
    if (range*4 != b[0]+b[1]+b[2]+b[3]) {
        return false;
    }
    if (range*2 != b[1]+b[2]) {
        return false;
    }
    return true;
}


int main() {
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }

    if (n==0) {
        cout << "YES" << endl;
        cout << 1 << endl;
        cout << 1 << endl;
        cout << 3 << endl;
        cout << 3 << endl;

    } else if (n==1) {
        cout << "YES" << endl;
        cout << 2*a[0] << endl;
        cout << 2*a[0] << endl;
        cout << 3*a[0] << endl;

    } else if (n==2) {
        sort(a, a+2);
        if (a[1] <= a[0]*3) {
            cout << "YES" << endl;
            cout << 4*a[0]-a[1] << endl;
            cout << 3*a[0] << endl;
        } else {
            cout << "NO" << endl;;
        }

    } else if (n==3) {
        for (int i = 1; i <= 10e6; ++i) {
            a[3] = i;
            if (check()) {
                cout << "YES" << endl;
                cout << i << endl;
                return 0;
            }
        }
        cout << "NO" << endl;

    } else if (n==4) {
        if (check()) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }

    return 0;
}