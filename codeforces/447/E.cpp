#include <vector>
#include <cmath>
#include <iostream>
using namespace std;

using namespace std;

#define l(x) (x<<1)
#define r(x) ((x<<1)+1)
#define MAX_N 300001
#define MOD 1000000009

int f[MAX_N], F[MAX_N], n, m;

void init() {
    f[1] = f[2] = 1;
    for (int i = 3; i < MAX_N; ++i) {
        f[i] = (f[i-2] + f[i-1]) % MOD;
    }
    F[1] = f[1];
    for (int i = 2; i < MAX_N; ++i) {
        F[i] = (F[i-1] + f[i])%MOD;
    }
}

// DT:data_type, data used to build the tree
// TT:tree_type, internal types of the nodes
// UT:update_type, must have operator += overloaded
template <class DT, class TT, class UT>
class SegmentTree{
public:
    // vector data is only used to build the tree
    // the tree doesn't update values in vector data
    // tt_def: tree type default, can be anything of type TT
    // ut_def: update type default, value that means 'no update', e.g. 0
    SegmentTree(const vector<DT> &data,
                TT tt_def = 0,
                UT ut_def = 0):
                    n(data.size()),
                    no_update(ut_def) {
        int tree_size = pow(2, ceil( log2(n) ) + 1);
        tree.assign(tree_size, tt_def);
        updates.assign(tree_size, ut_def);
        build_tree(data,1,0,n-1);
    }

    // Query for the range [i..j] inclusive
    // Behaviour is defined in function merge
    TT query(int i, int j){
        return query(i, j, 1, 0, n-1);
    }

    // Updates the range [i..j] with the value u
    void update(int i, int j, const UT& u){
        update(i, j, u, 1, 0, n-1);
    }

private:
    int n;
    vector<TT> tree;
    vector<UT> updates;
    UT no_update;  // update type default, used to init and reset updates

    // OVERRIDE
    // Take two children nodes and merge them to get the parent node
    // Here is defined the behaviour of the tree
    TT merge(const TT& a, const TT& b){
        return a+b;
    }

    // OVERRIDE
    // Convert a data_type value to a tree_type value
    // Used in the leaves of the tree
    TT to_node(const DT& d){
        return d;
    }

    // OVERRIDE
    // Update tree[v] with value u
    // u is not necessary equal to updates[v]
    void update_node(int v, const UT& u) {
        tree[v] += u;
    }


    void resolve(int v, int L, int R){
        if (updates[v] != no_update) {
            update_node(v, updates[v]);
            if (L != R) {
                updates[l(v)] += updates[v];
                updates[r(v)] += updates[v];
            }
            updates[v] = no_update;
        }     
    }

    void build_tree(const vector<DT> &data, int v, int L, int R){ // v:vertex
        if(L == R) {
            tree[v] = to_node(data[L]);
        }
        else{
            int m = (L+R)/2;
            build_tree(data, l(v), L, m);
            build_tree(data, r(v), m+1, R);
            tree[v] = merge(tree[l(v)], tree[r(v)]);
        }
    }

    TT query(int i, int j, int v, int L, int R){
        resolve(v, L, R);
        if(L>=i && R<=j){
            return tree[v];
        }
        int m = (L+R)/2;
        if(j <= m) {
            return query(i, j, l(v), L, m);
        }
        if(i > m) {
            return query(i, j, r(v), m+1, R);
        }
        return merge(query(i, j, l(v), L, m),
                     query(i, j, r(v), m+1, R));
    }
    
    void update(int i, int j, const UT& u, int v, int L, int R){
        resolve(v, L, R);
        if (L>=i && R<=j){
            update_node(v, u);
            if (L != R) {
                updates[l(v)] += u;
                updates[r(v)] += u;
            }
            return;
        }
        int m = (L+R)/2;
        if (i <= m) {
            update(i, j, u, l(v), L, m);
        }
        if (j > m) {
            update(i, j, u, r(v), m+1, R);
        }
        tree[v] = merge(tree[l(v)], tree[r(v)]);
    }
};




int main() {
    init();
    cin >> n >> m;
    vector<int> a(n, 0);
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }

    SegmentTree<int, int, pair<int, int> > st(a);

    return 0;
}