#include <cstring>

#include <iostream>
using namespace std;

int p, n;
int h[300];

int main() {
	cin >> p >> n;
	memset(h, -1, sizeof(h));
	for (int i = 0; i < n; ++i) {
		int x;
		cin >> x;
		if (h[x%p] != -1) {
			cout << i+1;
			return 0;
		} else {
			h[x%p] = x%p;
		}
	}
	cout << -1;
	return 0;
}