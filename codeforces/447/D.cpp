#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;

typedef long long ll;

int n, m, k, p;

int cols[1000] = {0}, rows[1000] = {0};
ll plea_till_row[1000001] = {0}, plea_till_col[1000001] = {0};

int main() {
	cin >> n >> m >> k >> p;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			int x;
			cin >> x;
			rows[i] += x;
			cols[j] += x;
		}
	}
	priority_queue<ll> cols_pq(cols, cols+m),
	rows_pq(rows, rows+n);

	for (int i = 0; i < k; ++i) {
		ll row = rows_pq.top();
		rows_pq.pop();
		plea_till_row[i+1] = plea_till_row[i] + row;
		rows_pq.push(row - m*p);

		ll col = cols_pq.top();
		cols_pq.pop();
		plea_till_col[i+1] = plea_till_col[i] + col;
		cols_pq.push(col - n*p);
	}

	ll pleasure = -1e14;
	for (int i = 0; i <= k; ++i) {
		pleasure = max(pleasure, plea_till_row[i] + plea_till_col[k-i] - 1LL*(k-i)*i*p);
	}
	cout << pleasure;
	return 0;
}