#include <iostream>
#include <string>
using namespace std;

string s;
int k;

int w[26];

int f(string s) {
	int suma = 0;
	for (int i = 0; i < s.size(); ++i) {
		suma += w[s[i]-'a']*(i+1);
	}
	return suma;
}

int main() {
	cin >> s >> k;

	int great_idx = 0;
	for(int i = 0; i < 26; ++i) {
		cin >> w[i];
		if (w[i] > w[great_idx]) {
			great_idx = i;
		}
	}

	char c = 'a' + great_idx;
	s.insert(s.end(), k, c);
	
	cout << f(s);

	return 0;
}