#include <iostream>
#include <string>
using namespace std;

bool beautiful(string& s) {
	int open=0, close=0;
	for (int i = 0; i < s.size(); ++i) {
		if (s[i] == '(') {
			open++;
		} else {
			close++;
		}
		if (close > open) {
			return false;
		}
	}
	return close == open;
}

int main() {
	string s;
	cin >> s;

	int num_open = 0,
	num_close = 0,
	num_sharp = 0;
	for (int i = 0; i < s.size(); ++i) {
		if (s[i] == '(') {
			num_open++;
		} else if (s[i] == ')') {
			num_close++;
		} else {
			num_sharp++;
		}
	}
	if (num_sharp + num_close > num_open) {
		cout << -1;
		return 0;
	}
	string ret;
	int replaced_sharps = 0,
	last_sharp = (num_open - num_close) - (num_sharp - 1);	// how many ')' should be in the last sharp
	for (int i = 0; i < s.size(); ++i) {
		if (s[i] != '#') {
			ret += s[i];
		} else {
			if (replaced_sharps == num_sharp-1) {
				for (int j = 0; j < last_sharp; ++j) {
					ret += ')';
				}
			} else {
				ret += ')';
			}
			replaced_sharps++;
		}
	}

	if (beautiful(ret)) {
		for (int i = 0; i < num_sharp - 1; ++i) {
			cout << 1 << endl;
		}
		cout << last_sharp << endl;
	} else {
		cout << -1;
	}
	return 0;
}