#include <iostream>
using namespace std;

int n, k;
int towers[101];    // start in 1
int movements[10001][2];    // start in 0

// return position of max
int get_max() {
    int ret = 1;
    for (int i=2; i<=n; ++i) {
        if (towers[i] > towers[ret]) {
            ret = i;
        }
    }
    return ret;
}

int get_min() {
    int ret = 1;
    for (int i=2; i<=n; ++i) {
        if (towers[i] < towers[ret]) {
            ret = i;
        }
    }
    return ret;
}

int calculate_instability(int maxi=-1, int mini=-1) {
    if (maxi==-1) {
        maxi = get_max();
    }
    if (mini==-1) {
        mini = get_min();
    }
    return towers[maxi] - towers[mini];
}

int main () {
    cin >> n >> k;
    for (int i=1; i<=n; ++i) {
        cin >> towers[i];
    }

    int min_instability = 999999999;
    int moves = -1;
    int checkpoint = -1;    // last time we made a reduction
    for (int i=0; i<k; ++i) {
        int maxi = get_max();
        int mini = get_min();
        if (maxi == mini) {
            min_instability = 0;
            moves = i;
            break;
        }
        movements[i][0] = maxi;
        movements[i][1] = mini;
        towers[mini]++;
        towers[maxi]--;
        int current_instability = calculate_instability();
        if (current_instability < min_instability && current_instability >= 0) {
            min_instability = current_instability;
            checkpoint = -1;
        } else if (current_instability == min_instability) {
            if (checkpoint==-1) {
                checkpoint = i;
            }
        } else {
            moves = i;
            break;
        }
    }
    if (moves == -1) {
        moves = k;
    }
    if (checkpoint!=-1) {
        moves = checkpoint;
    }

    cout << min_instability << " " << moves << endl;
    for (int i=0; i<moves; ++i) {
        cout << movements[i][0] << " " << movements[i][1] << endl;
    }

    return 0;
}