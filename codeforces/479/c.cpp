#include <iostream>
#include <algorithm>
using namespace std;

int n, a, b;

class Exam {
public:
    int a;
    int b;
    bool operator<(const Exam& e) const{
        if (a < e.a) {
            return true;
        } else if (a==e.a) {
            if (b < e.b) {
                return true;
            }
        }
        return false;
    }
};

Exam exams[5001];

int main() {
    cin >> n;
    for (int i=0; i<n; ++i) {
        cin >> exams[i].a >> exams[i].b;
    }
    sort(exams, exams+n);
    int cur_day = min(exams[0].a, exams[0].b);
    for (int i=1; i<n; ++i) {
        if (exams[i].b < cur_day) {
            cur_day =  exams[i].a;
        } else {
            cur_day = exams[i].b;
        }
    }
    cout << cur_day;
    return 0;
}