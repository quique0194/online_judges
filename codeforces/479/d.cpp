#include <iostream>
#include <algorithm>
using namespace std;


int n, l, x, y;
int marks[100001];

int main() {
    cin >> n >> l >> x >> y;
    for (int i=0; i<n; ++i) {
        cin >> marks[i];
    }
    // case we don't have to draw any mark
    bool foundx = false;
    bool foundy = false;
    bool foundxy = false;
    int middle_mark;
    for (int i=0; i<n && (!foundy || !foundx); ++i) {
        if (binary_search(marks, marks+n, marks[i]+x)) {
            foundx = true;
        }
        if (binary_search(marks, marks+n, marks[i]+y)) {
            foundy = true;
        }
        if (binary_search(marks, marks+n, marks[i]+x+y)) {
            foundxy = true;
            middle_mark = marks[i]+x;
        }
        if (binary_search(marks, marks+n, marks[i]+x-y) && marks[i]+x <= l) {
            foundxy = true;
            middle_mark = marks[i]+x;
        }
        if (binary_search(marks, marks+n, marks[i]-x+y) && marks[i]-x >= 0) {
            foundxy = true;
            middle_mark = marks[i]-x;
        }
    }
    if (foundx && foundy) {
        cout << 0;
        return 0;
    }
    // case we have to do only one mark
    if (foundx) {
        cout << 1 << endl << y;
        return 0;
    }
    if (foundy) {
        cout << 1 << endl << x;
        return 0;
    }
    if (foundxy) {
        cout << 1 << endl << middle_mark;
        return 0;
    }
    // case we have to do two marks
    cout << 2 << endl << x << " " << y;
    return 0;
}