#include <iostream>

using namespace std;

int main() {
    int a, b, c;
    cin >> a >> b >> c;
    int res[6] = {
        a*b*c,
        a+b+c,
        (a+b)*c,
        a*(b+c),
        a+b*c,
        a*b+c
    };
    int rpta = -9999999;
    for (int i = 0; i < 6; ++i) {
        if (res[i] > rpta) {
            rpta = res[i];
        }
    }
    cout << rpta;
    return 0;
}