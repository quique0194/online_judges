#include <iostream>
#include <cstdlib>
using namespace std;

#define MOD 1000000007

typedef int ll;

ll table[5001][5001];   // dp table[journey][floor]
int n, a, b, k;
ll S[5001];             // suma parcial

void calc_S(int row) {
    S[0] = 0;
    for (int j=1; j<=n; ++j) {
        S[j] = (S[j-1] + table[row][j]) % MOD;
    }
}

int main() {
    cin >> n >> a >> b >> k;
    // caso base
    for (int i=1; i<=n; ++i) {
        table[k][i] = 1;
    }
    // bottom-up
    for (int i=k-1; i>=0; --i) {
        calc_S(i+1);
        for (int j=1; j<=n; ++j) {
            int dist = abs(b-j)-1;
            table[i][j] = (S[min(n, j+dist)] - S[max(0, j-dist-1)]) % MOD;
            table[i][j] -= (table[i+1][j]);
            table[i][j] %= MOD;
        }
    }
    // rpta
    if (table[0][a] < 0) {
        table[0][a] += MOD;
    }
    cout << table[0][a] << endl;
    return 0;
}