#include <iostream>

using namespace std;

typedef long long ll;
ll t, n, l, r;

int main()
{
    cin >> t;
    while(t--){
        cin >> n >> l >> r;
        ll x = n/l;
        ll max = r*x;
        if( n <= max )
            cout << "Yes" << endl;
        else
            cout << "No" << endl;
    }
    return 0;
}
