#include <iostream>
#include <algorithm>
using namespace std;

int n;
int a[100001];
int l[100001];
int r[100001];

int main(){
    cin >> n;
    for(int i=0; i<n; ++i) cin >> a[i];

    l[0] = 1;
    for(int i=1; i<n; ++i){
        if(a[i]>a[i-1])
            l[i] = l[i-1] + 1;
        else
            l[i] = 1;
    }

    r[n-1] = 1;
    for(int i=n-2; i>=0; --i){
        if(a[i]<a[i+1])
            r[i] = r[i+1] + 1;
        else
            r[i] = 1;
    }

    int ans=1;
    for(int i=0; i<n; ++i){
        ans = max(ans, l[i]);
    }
    ans = min(n, ans+1);

    for(int i=1; i<n-1; ++i){
        if(a[i-1]+1<a[i+1])
            ans = max(ans, l[i-1] + 1 + r[i+1]);
    }

    cout << ans << endl;
    return 0;
}
