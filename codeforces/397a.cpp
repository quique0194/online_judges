#include <iostream>

using namespace std;

int n, l, r;
bool segment[100] = {0};

int main()
{
    cin >> n;
    cin >> l >> r;
    for(int i=l; i<r; ++i)
        segment[i] = 1;
    n--;
    while(n--){
        cin >> l >> r;
        for(int i=l; i<r; ++i)
            segment[i] = 0;
    }
    int sum = 0;
    for(int i=0; i<100; ++i)
        sum += segment[i];
    cout << sum << endl;
    return 0;
}
