#include <cmath>
#include <cstring>
#include <iostream>
using namespace std;

#define MOD 1000000007

typedef int ll;
int r, g, h;

int height(int blocks) {
    double t = blocks*2;
    int c = floor(sqrt(t));
    if (c*(c+1) <= t) {
        return c;
    } else {
        return c-1;
    }
}

int f(int x) {
    return x*(x+1)/2;
}

// red blocks = bock(level) - green blocks;
int blocks(int level) {
    return r+g - (f(h)-f(level));
}

ll memo[200001][894];


ll dp(int green, int level) {
    if (level==0) {
        return 1;
    }
    if (memo[green][level]!=-1) {
        return memo[green][level];
    }
    ll sum = 0;
    if (green >= level) {
        sum += dp(green-level, level-1);
        sum %= MOD;
    }
    int red = blocks(level)-green;
    if (red >= level) {
        sum += dp(green, level-1);    
        sum %= MOD;
    }
    return memo[green][level] = sum;
}

int main() {
    memset(memo, -1, sizeof(memo));
    cin >> r >> g;
    h = height(r+g);
    cout << dp(g, h);
    return 0;
}