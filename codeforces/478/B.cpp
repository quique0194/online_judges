#include <iostream>

using namespace std;

typedef long long ll;
ll n, m, kmax, kmin;

ll f (ll n) {
    // n: members in the groups
    return n*(n-1)/2;
}

int main () {
    cin >> n >> m;

    kmax = f(n-m+1);
    kmin = (m-(n%m))*f(n/m) + (n%m)*f(n/m+1);

    cout << kmin << " " << kmax << endl;

    return 0;
}