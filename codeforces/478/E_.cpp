#include <iostream>
using namespace std;

#define LIM 100000000000000

typedef long long ll;

ll n, k;

bool is_wavy(string n) {
    for (int i = 1; i < n.size()-1; ++i) {
        if ( (n[i] <= n[i-1] || n[i] <= n[i+1]) &&
             (n[i] >= n[i-1] || n[i] >= n[i+1])) {
            return false;
        }
    }
    return true;
}

string lltos(ll n) {
    string ret;
    while (n) {
        char c = n%10 + '0';
        ret += c;
        n /= 10;
    }
    return string(ret.rbegin(), ret.rend());
}

int main() {
    cin >> n >> k;
    int cont = 0;
    for (ll i = n*k; i<=LIM; i+=n) {
        if (is_wavy(lltos(i))) {
            cont++;
            if (cont == k) {
                cout << i;
                return 0;
            }
        }
    }
    cout << -1;
    return 0;
}