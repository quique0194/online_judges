#include <iostream>
#include <algorithm>
using namespace std;

typedef long long ui;

ui g[3];

int main() {
    cin >> g[0] >> g[1] >> g[2];

    sort(g, g+3);
    cout << min( (g[0]+g[1]+g[2])/3, g[0]+g[1] );

    return 0;
}