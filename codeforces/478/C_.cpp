#include <iostream>
#include <algorithm>
using namespace std;

typedef unsigned int ui;

ui g[3];

int main() {
    cin >> g[0] >> g[1] >> g[2];

    ui cont = 0;
    while(true) {
        sort(g, g+3);
        if (g[2]==g[1] && g[1]==g[0]) {
            cont += g[0];
            break;
        } else if (g[2] >= 2 && g[1] >= 1) {
            g[2] -= 2;
            g[1] -= 1;
            cont++;
        } else {
            break;
        }
    }
    cout << cont;
    return 0;
}