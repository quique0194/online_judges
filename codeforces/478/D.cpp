#include <cmath>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define MOD 1000000007

typedef int ll;
int r, g, h;

int height(int blocks) {
    double t = blocks*2;
    int c = floor(sqrt(t));
    if (c*(c+1) <= t) {
        return c;
    } else {
        return c-1;
    }
}

int f(int x) {
    return x*(x+1)/2;
}

// red blocks = bock(level) - green blocks;
int blocks(int level) {
    return r+g - (f(h)-f(level));
}

ll dp[2][200001];

void print_array(int *a, int size) {
    for (int i = 0; i < size; ++i) {
        cout << a[i] << " ";
    }
    cout << endl;
}

int main() {
    cin >> r >> g;
    h = height(r+g);

    // init dp
    for (int i = 0; i <= g; ++i) {
        dp[0][i] = 1;
    }

    // dp
    int prev = 0, cur = 1;
    for(int i = 1; i <= h; ++i) {
        memset(dp[cur], 0, sizeof(dp[cur]));
        for (int j = 0; j <= g-i; ++j) {
            dp[cur][j+i] += dp[prev][j];
            dp[cur][j+i] %= MOD;
        }
        int red = blocks(i);
        // cout << "red: " << red << endl;
        for (int j = 0; j <= min(red-i, g); ++j) {
            dp[cur][j] += dp[prev][j];
            dp[cur][j] %= MOD;
        }
        // print_array(dp[cur], g+1);
        swap(prev, cur);
    }

    cout << dp[prev][g];
    return 0;
}